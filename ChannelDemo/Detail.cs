﻿using System;
using System.Collections.Generic;
using System.Data;
using SkiaSharp;

namespace ChannelDemo
{
    public partial class Program
    {
        private static void DrawAttribution(SKCanvas canvas)
        {
            const string text = "(c) OpenStreetMap contributors";
            using (var paint = new SKPaint())
            {
                paint.TextSize = 12;
                paint.Color = new SKColor(255, 255, 255, 225);
                var width = paint.MeasureText(text);
                canvas.DrawRect(0, 495, width + 10, 17, paint);

                paint.IsAntialias = true;
                paint.Color = SKColors.Black;
                canvas.DrawText(text, 5, 508, paint);
            }
        }
    }

    public class Tile
    {
        public int X { get; }
        public int Y { get; }
        public string Url { get; }

        public Tile(int x, int y, string url)
        {
            X = x;
            Y = y;
            Url = url;
        }

        public override string ToString() => $"({X}/{Y})";
    }
}