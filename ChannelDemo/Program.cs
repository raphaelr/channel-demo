﻿using System;
using System.IO;
using System.Net.Http;
using System.Threading.Channels;
using System.Threading.Tasks;
using SkiaSharp;

namespace ChannelDemo
{
    public partial class Program
    {
        public static async Task Main()
        {
            var tiles = new[]
            {
                new Tile(0, 0, "https://a.tile.openstreetmap.org/6/62/38.png"),
                new Tile(1, 0, "https://a.tile.openstreetmap.org/6/63/38.png"),
                new Tile(0, 1, "https://a.tile.openstreetmap.org/6/62/39.png"),
                new Tile(1, 1, "https://a.tile.openstreetmap.org/6/63/39.png")
            };

            var httpClient = new HttpClient(new HttpClientHandler { MaxConnectionsPerServer = 2 });
            var channel = Channel.CreateUnbounded<(Tile, SKImage)>(new UnboundedChannelOptions
            {
                // Both options default to false
                SingleReader = true,
                SingleWriter = false
            });
            foreach (var tile in tiles)
            {
                DownloadTile(channel.Writer, httpClient, tile);
            }

            using (var surface = SKSurface.Create(new SKImageInfo(512, 512, SKColorType.Rgba8888)))
            {
                for (var i = 0; i < tiles.Length; i++)
                {
                    // Wait for the next tile
                    var (tile, image) = await channel.Reader.ReadAsync();

                    Console.WriteLine($"Drawing {tile}");
                    surface.Canvas.DrawImage(image, tile.X * 256, tile.Y * 256);
                    image.Dispose();
                }

                DrawAttribution(surface.Canvas);

                using (var snapshot = surface.Snapshot())
                using (var data = snapshot.Encode())
                using (var file = File.OpenWrite("NorthIsland.png"))
                {
                    data.SaveTo(file);
                }
            }
        }

        private static async void DownloadTile(ChannelWriter<(Tile, SKImage)> writer, HttpClient httpClient, Tile tile)
        {
            Console.WriteLine($"Starting download of {tile}");

            // We're in an async void method, so we better not let an exception go unobserved.
            try
            {
                var bytes = await httpClient.GetByteArrayAsync(tile.Url);
                var image = SKImage.FromEncodedData(bytes);
                Console.WriteLine($"Downloaded {tile}");

                // Send the tile to the reader
                await writer.WriteAsync((tile, image));
            }
            catch
            {
                // Exception handling is left as an exercise to the reader
                await writer.WriteAsync((tile, null));
            }
        }
    }
}
