Demo of the System.Threading.Channels namespace in .NET Core.
See [tapesoftware.net/channels](https://tapesoftware.net/channels/) for more information.

![North island](NorthIsland.png)
